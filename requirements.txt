Django==1.6.2
Pillow==2.5.1
djangorestframework==2.3.14
stevedore==0.15
virtualenv==1.9.1
virtualenv-clone==0.2.5
virtualenvwrapper-win==1.1.5
wheel==0.24.0
