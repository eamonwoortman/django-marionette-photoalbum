﻿define(["marionette", "core/vent", "core/app"], function (Marionette, Vent, App) {
    function initModules(modules) {
        App.modules = {};

        var modPaths = _.map(modules, function (module) {
            return "modules/" + module + "/setup";
        });
        require(modPaths, function () {
            Vent.trigger("modules:init:before");
            _.each(arguments, function (Module, index) {
                var moduleName = modules[index];
                App.modules[moduleName] = new Module({App: App, Vent: Vent});
            });
            Vent.trigger("modules:init:after");
        });
    }

    return {
        init: initModules
    };
});