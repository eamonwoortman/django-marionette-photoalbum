// Filename: router.js
define(["marionette", "./controller", "../vent"], function (Marionette, controller, appVent) {
    var router = new Marionette.AppRouter({
        appRoutes: {}, //reqiored to avoid the 'Object.keys called on non-object' error in marionette's app router
        controller: controller
    });

    var routeTable = Object.create(null);

    function dispatchRoute(routeName) {
        return function () {
            var args = arguments;
            appVent.trigger("route:changed", routeName, Backbone.history.fragment);
            _.each(routeTable[routeName], function (callbackFn) {
                callbackFn.apply(null, args);
            });
        }
    }

    function addRoute(route, name, callback) {
        if (!_.has(routeTable, route)) {
            routeTable[route] = [];
            router.route(route, name, dispatchRoute(route));
        }

        if (callback) {
            routeTable[route].push(callback);
        }
    }

    function handleAddRoute(route, name, callback) {
        if (_.isObject(route)) {
            _.each(route, function (config, routeString) {
                addRoute(routeString, config.name, config.fn);
            });
        }
        else {
            addRoute(route, name, callback);
        }
    }

    router.on("all", controller.all);
    router.listenTo(appVent, "route:add", handleAddRoute);
    return router;
});