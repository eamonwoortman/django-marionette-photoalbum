﻿define(["marionette", "core/vent"], function (Marionette, appVent) {
    return {
        init: function (app) {
            appVent.on("layout:show", function (section, view) {
                var appSection = app[section];

                if (appSection) {
                    appSection.show(view);
                }
                $(document).foundation('reflow');
                $(document).foundation('equalizer','reflow');
            });
        }
    };
});