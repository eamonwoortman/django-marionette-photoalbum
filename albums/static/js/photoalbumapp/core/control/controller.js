// Controller
define(["jquery", "marionette", "core/vent", "core/app"], function ($, Marionette, Vent, App) {
    //event managment

    Vent.on("route:action", function (action, options) {
        Backbone.history.navigate(action, options);
    });

    return {
        all: function (route) {
            if (route != "game") {
                $("page").css({ height: 600 });
                $("dContiner").css({ height: 600 });
            }

            App.toggleOverlay(false);

            return;
        }
    };
});
