﻿// Main app file
define(["jquery", "underscore", "marionette", "./vent"], function ($, _, Marionette, appVent) {
    var app = new Marionette.Application(),
        core, modules;

    app.addRegions({
        navigation: "#navigation",
        content: "#content"
    });

    core = ["core/control/router", "core/control/controller", "core/control/modules", "core/control/layout"];
    //only modules directory names
    modules = ["navigation", "photoalbum"];

    app.addInitializer(function () {
        require(core, function (router, controller, modCtrl, layout) {
            layout.init(app);
            modCtrl.init(modules);
        });
    });
    
    appVent.on("modules:init:after", function () {
        appVent.trigger("history:start:before");
        Backbone.history.start({ pushState: false });
        appVent.trigger("history:start:after");

        //now load Foundation
        $(document).foundation();
    });

    return _.extend(app, {
        toggleOverlay: function (show) {
            var overlay = $("#itp_overlay");
            if (show) {
                overlay.show();
            } else {
                overlay.hide()
            }
        }
    });
});
