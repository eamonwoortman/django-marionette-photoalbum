﻿// Main Event Aggregator
define(['marionette'], function (Marionette) {
    return new Backbone.Wreqr.EventAggregator();
});
