// Starting point for our etlive app

require.config({
    urlArgs: 'bust=' + (new Date()).getTime(), /* used for development environment, forcing a fresh version */
    enforceDefine: false, /* changed this to false since the foundation components aren't AMD */
    paths: {
        'jquery': 'lib/jquery',
        'underscore': 'lib/underscore-amd',
        'backbone': 'lib/backbone',
        'backbone.paginator': 'lib/backbone.paginator',
        'marionette': 'lib/backbone.marionette',

        /* Foundation */
        'foundation.core': 'lib/foundation/foundation',
        'foundation.abide': 'lib/foundation/foundation.abide',
        'foundation.accordion': 'lib/foundation/foundation.accordion',
        'foundation.alert': 'lib/foundation/foundation.alert',
        'foundation.clearing': 'lib/foundation/foundation.clearing',
        'foundation.dropdown': 'lib/foundation/foundation.dropdown',
        'foundation.equalizer': 'lib/foundation/foundation.equalizer',
        'foundation.interchange': 'lib/foundation/foundation.interchange',
        'foundation.joyride': 'lib/foundation/foundation.joyride',
        'foundation.magellan': 'lib/foundation/foundation.magellan',
        'foundation.offcanvas': 'lib/foundation/foundation.offcanvas',
        'foundation.orbit': 'lib/foundation/foundation.orbit',
        'foundation.reveal': 'lib/foundation/foundation.reveal',
        'foundation.tab': 'lib/foundation/foundation.tab',
        'foundation.tooltip': 'lib/foundation/foundation.tooltip',
        'foundation.topbar': 'lib/foundation/foundation.topbar',

        'text': 'lib/text'
    },
    shim: {
        'underscore': {
            exports: '_'
        },

        'backbone': {
            deps: ["underscore", "jquery"],
            exports: "Backbone"
        },

        'backbone.paginator': {
            deps: ["backbone"],
            exports: "BackbonePaginator"
        },

        'marionette': {
            deps: ["backbone"],
            exports: "Marionette"
        },

        /* Foundation */
        'foundation.core': {
            deps: [
            'jquery'
            ],
            exports: 'Foundation'
        },
        'foundation.abide': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.accordion': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.alert': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.clearing': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.dropdown': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.equalizer': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.interchange': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.joyride': {
            deps: [
            'foundation.core',
            'foundation.cookie'
            ]
        },
        'foundation.magellan': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.offcanvas': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.orbit': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.reveal': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.tab': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.tooltip': {
            deps: [
            'foundation.core'
            ]
        },
        'foundation.topbar': {
            deps: [
            'foundation.core'
            ]
        }

    }
});

define(['marionette', 'jquery', 'foundation.equalizer', 'foundation.reveal', 'foundation.orbit', 'backbone.paginator'], function (Marionette, $) {
    require(['core/app'], function (app) {
        app.start();
    });
});