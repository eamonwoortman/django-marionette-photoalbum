﻿define(["marionette", "./vent", "./views/albumOverview", "./views/photoOverview", "./views/photoDetails", "./collections/albums", "./collections/images", './models/image'],
    function (Marionette, modVent, AlbumOverview, PhotoOverview, PhotoDetails, AlbumCollection, ImageCollection, ImageModel) {
        /**
         * Main entry point for the module
         * handles initialization, communication with the outside world
         * (such as showing the layout, listening for app events and delegating them to the module)
         */
        return Marionette.Controller.extend({
            defaultRoute: function () {
                Backbone.history.navigate("home", true);
            },
            initialize: function (options) {
                var appVent = options.Vent;
                appVent.trigger("route:add", "", "default", this.defaultRoute);
				
				appVent.trigger("route:add", "home", "default", function () {
					var albumCollection = new AlbumCollection();
                    albumCollection.fetch();

                    var albumOverview = new AlbumOverview({collection : albumCollection});
					appVent.trigger("layout:show", "content", albumOverview);
				});

				appVent.trigger("route:add", "album/:id/", "album", function (id) {
                    var imageCollection = new ImageCollection();
                    imageCollection.fetch({data: { album_id: id }});

					var photoOverview = new PhotoOverview({collection : imageCollection});
					appVent.trigger("layout:show", "content", photoOverview);
				});

				appVent.trigger("route:add", "photo/:id/", "photo", function (id) {
                    var image = new ImageModel({id: id});
					var photoDetails = new PhotoDetails({model: image});
					var fetching = image.fetch();
                    //let's wait until the model has been fetched
                    fetching.done(function() {
                        appVent.trigger("layout:show", "content", photoDetails);
                    });


				});
            }
        });
});