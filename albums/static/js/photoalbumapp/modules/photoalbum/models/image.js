define([
  'underscore',
  'backbone',
], function(_, Backbone) {
    return Backbone.Model.extend({
        urlRoot: 'api/images/',
/*
        defaults: function () {
            return {
                id: -1,
                title: 'N/A',
                image: 'N/A',
                created: '2014-07-26T13:30:16.141Z',
                width: -1,
                height: -1,
                user: -1
            };
        }
*/
    });
});
