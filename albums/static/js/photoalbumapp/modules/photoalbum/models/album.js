define([
  'underscore',
  'backbone',
], function(_, Backbone) {
    return Backbone.Model.extend({
        url: function() { 
            return '/api/albums';
        },

        defaults: function () {
            return {
                title: 'N/A',
                created: '2014-07-26T13:30:16.141Z',
            };
        },

        
  });
});
