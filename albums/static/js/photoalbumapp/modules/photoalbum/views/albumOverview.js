define([
  'jquery',
  'underscore',
  'backbone',
  '../models/album',
  '../collections/albums',

  '../../shared/views/paginatedview',
  'text!../templates/album_overview.html',
  'text!../templates/album_overview_table.html'
], function($, _, Backbone, AlbumModel, AlbumCollection, PaginatedView, albumOverviewTemplate, albumTableTemplate){
    var AlbumOverviewItemView = Backbone.Marionette.ItemView.extend({
        model: AlbumModel,
        tagName: "tr",
        getTemplate: function () {
            var templ = "<td><%= id %></td><td><%= title %></td><td><%=num_images%></td><td><%= created %></td><td><a href='#album/<%= id %>/'>Show</a></td>";
            return _.template(templ);
        },
      });

	var AlbumTableView = Marionette.CompositeView.extend({
        template: _.template(albumTableTemplate),

		childView: AlbumOverviewItemView,
        childViewContainer: "tbody",
	});

    var AlbumOverview = PaginatedView.extend({
		template: _.template(albumOverviewTemplate),
     
        getMainView: function() {
            return AlbumTableView;
        },

        submitAddAlbum: function (form, modal) {
            var collection = this.collection;
            var inputText = form.find("input").val();


            var album = new AlbumModel();
            var albumDetails = {
                title: inputText
            }         
            album.save(albumDetails, {
                success: function (model, respose, options) {
                    collection.add(model);
                    collection.trigger('refresh');
                    modal.foundation('reveal', 'close');
                },
                error: function (model, response, options) {
                    if (response.status == 403) {
                        alert("Please log out from the admin site before adding new records in the frontend");
                        return;
                    }
                    alert("Something went wrong while updating the model, error message: " + response.responseText);
                }
            });
        },

        onShow: function () {
            var hooked = false;
            var modal = $('#addAlbumModal');
            var self = this;
            modal.on('open.fndtn.reveal', function () {
                var addAlbumForm = $('#addAlbumForm');
                if (hooked) {
                    return;
                }
                var ourModal = $(this);
                addAlbumForm.submit(function (event) {
                    event.preventDefault();
                    self.submitAddAlbum(addAlbumForm, ourModal);
                });

                hooked = true;
            });   
        }
	});


    return AlbumOverview;
});
