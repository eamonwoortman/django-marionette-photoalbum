define([
  'jquery',
  'underscore',
  'backbone',
  '../models/image',
  '../../shared/views/paginatedview',
  'text!../templates/photo_overview.html'
], function($, _, Backbone, ImageModel, PaginatedView, photoOverviewTemplate){
    var ImageItemView = Backbone.Marionette.ItemView.extend({
        model: ImageModel,
        tagName: "li",
        getTemplate: function () {
            var templ = '<a href="#photo/<%= id %>/"><img class="th" src="/media/<%= image %>"></a>';
            return _.template(templ);
        },

    });
    var EmptyItemView = Backbone.Marionette.ItemView.extend({
        template: function() {
            return "No photos found";
        }
    });
	var ImageGridView = Marionette.CollectionView.extend({
        tagName: 'ul',
        className: 'large-block-grid-4',
		childView: ImageItemView,
        emptyView: EmptyItemView,
	});

    var PhotoOverview = PaginatedView.extend({
		template : _.template(photoOverviewTemplate),
    
        getMainView: function() {
            return ImageGridView;
        },
	});
    return PhotoOverview;
});
