define([
  'jquery',
  'underscore',
  'backbone',
  'text!../templates/photo_details.html'
], function($, _, Backbone, photoDetailsTemplate){
	return Marionette.ItemView.extend({
		tagName: 'div',
        template: function(serializedData) {
            return _.template(photoDetailsTemplate, serializedData)
        },
	});
});
