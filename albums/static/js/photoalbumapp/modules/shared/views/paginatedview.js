define([
  'jquery',
  'underscore',
  'backbone',
  'text!../templates/pagination_controls.html'
], function($, _, Backbone, paginationControlsTemplate){
    var PaginationControls = Marionette.ItemView.extend({
        className: "pagination-centered",
        template: _.template(paginationControlsTemplate),
        events: {
            "click a[class=navigatable]": "navigateToPage"
        },

        initialize: function(options){
            this.paginatedCollection = options.paginatedCollection;
            //make sure this view gets rerendered when the collection is refreshed
            this.paginatedCollection.on("sync", this.render);
        },

        serializeData: function(){
            var state = _.clone(this.paginatedCollection.state);
            //defaults when there's no collection yet.
            if(state.totalPages === undefined) {
                state.totalPages = 0;
            } else {
                var pageSet = [];
                for(var i = state.firstPage; i < state.totalPages + 1; i++) {
                    pageSet.push(i);
                }
                state.pageSet = pageSet;
            }
            return state;
        },

        navigateToPage: function(e){
            e.preventDefault();
            var page = parseInt($(e.target).data("page"), 10);
            this.trigger("page:change", page);
        },

    });

    var PaginatedView = Backbone.Marionette.LayoutView.extend({
        currentPage: 1,
        template: "#paginated-view",
        regions: {
            paginationControlsRegion: "#pagination-controls",
            paginationMainRegion: "#pagination-main"
        },
        changePage: function (page) {
            this.currentPage = page;
            this.collection.getPage(page);
        },
        getMainView: function() {
            return Backbone.Marionette.ItemView;
        },

        initialize: function(options){
            var that = this;
            this.collection = options.collection;

            var controls = new PaginationControls({
                paginatedCollection: this.collection
            });
            controls.on("page:change", function(page) {
                that.changePage(page);
            });
            var MainViewClass = this.getMainView();
            var listView = new MainViewClass({
                collection: this.collection
            });
            this.collection.on("refresh", function () {
                that.changePage(that.currentPage);
            });
            this.on("show", function(){
                this.paginationControlsRegion.show(controls);
                this.paginationMainRegion.show(listView);
            });
        }
    });
    return PaginatedView;
});
