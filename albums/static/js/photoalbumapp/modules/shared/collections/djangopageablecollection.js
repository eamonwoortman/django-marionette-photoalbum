define([
  'underscore',
  'backbone',
], function(_, Backbone){
	/*********
	* A pagabale collection compatible with Django's Rest Framework
	*********/
    return Backbone.PageableCollection.extend({
        // Name of the attribute containing the array of records
        resultsField: 'results',
        totalRecordsField: 'count',
        nextField: 'next',
        previousField: 'previous',


        // You can configure the mapping from a `Backbone.PageableCollection#state`
        // key to the query string parameters accepted by your server API.
        queryParams: {
            // `Backbone.PageableCollection#queryParams` converts to ruby's
            // will_paginate keys by default.
            currentPage: "page",
            pageSize: "page_size",
            totalRecords: "count",
        },

        parseRecords : function(resp, options) {
            if (resp && _.has(resp, this.resultsField) && _.isArray(resp[this.resultsField])) {
                return resp[this.resultsField];
            } else {
                return Backbone.PageableCollection.prototype.parseRecords.apply(this, arguments);
            }
        },

        parseState: function(resp, queryParams, state, options) {
            return state = {
                totalRecords: resp[this.totalRecordsField]
            };
        },

        parseLinks: function(resp, options) {
            var links;
            return links = {
                prev: resp[this.previousField],
                next: resp[this.nextField],
                first: null
            };
        },
    });
});