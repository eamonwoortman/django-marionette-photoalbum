﻿define(["marionette", "../vent", "../widgets/breadcrumbs"], function (Marionette, modVent, pagesNav) {
    var ctrl = Marionette.Controller.extend({
        initialize: function () {
            this.breadcrumbs = new Backbone.Collection();
            this.handlePagesView();
        },
        handlePagesView: function () {
            console.log("hnadlePagesView");
            /*this.listenTo(pagesNav, "menu:item:clicked", this.onMenuItemClicked);
            this.listenTo(sessionNav, "menu:item:clicked", this.onMenuItemClicked);
            this.listenTo(modVent, "route:changed", this.onRouteChanged);
            */
        },



        onRouteChanged: function (route, urlFragment) {
            pagesNav.collection.forEach(function (model) {
                //works for now, it should be a more precise match imo
                var isMatch = urlFragment.indexOf(model.get('action')) !== -1;
                model.setActive(isMatch);
            });
        },
        onMenuItemClicked: function (menuItem) {
            modVent.trigger("route:action", menuItem.model.get("action"));
        }
    });

    return new ctrl;
});