﻿define(["backbone", "../models/breadcrumb"], function (Backbone, BreadcrumbModel) {
    var BreadcrumbCollection = Backbone.Collection.extend({
        routeTable : new Backbone.Collection([
            { action: "home", title: "Home", url: "#" },
            { action: "album/:id/", title: "Album", parent: 'home' },
            { action: "photo/:id/", title: "Photo", parent: 'album/:id/' }
        ], {model: BreadcrumbModel}),
        model: BreadcrumbModel,

        getRouteTableRoute: function(action) {
            return this.routeTable.where({ action: action })[0];
        }
    });

    return BreadcrumbCollection;
});