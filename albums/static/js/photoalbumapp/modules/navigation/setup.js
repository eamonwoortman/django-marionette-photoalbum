﻿define(["marionette", "./vent", "text!./templates/layout.html", "./collections/breadcrumbs", "./models/breadcrumb"/*, "./controllers/main"*/],
    function (Marionette, modVent, layoutTpl, BreadcrumbCollection, BreadCrumbModel/*, mainController*/) {
        var BreadcrumbItemView = Backbone.Marionette.ItemView.extend({
            template: function(serializedData) {
                return _.template('<a href="<%=url%>"><%=title%></a>', serializedData);
            },
            tagName: 'li',
            model: BreadcrumbCollection.model,
        });

        // Setting up the layout
        function makeLayout() {
            return Backbone.Marionette.CollectionView.extend({
                initialize: function() {
                    this.collection.on("sync", function() { console.log("foobaaar") });
                },
                tagName: 'ul',
                className: 'breadcrumbs',
                childView: BreadcrumbItemView
            });
        }

        /**
         * Main entry point for the module
         * handles initialization, communication with the outside world
         * (such as showing the layout, listening for app events and delegating them to the module)
         */
        return Marionette.Controller.extend({

            onRouteChange: function(route, url) {
                var breadcrumb = this.collection.getRouteTableRoute(route);
                if(breadcrumb == undefined)
                    return;

                //store the url for later use
                breadcrumb.set('url', '#' + url);

                //remove all breadcrumbs which aren't the parent of the new breadcrumb
                for (var i = this.collection.length - 1; i > -1; i -=1){
                    model = this.collection.models[i];
                    var action = model.get('action');
                    if(action != breadcrumb.get('parent') && !model.isRootNode()) {
                        this.collection.remove(model);
                    }
                };

                //add the new breadcrumb unless it's the root node
                if(!breadcrumb.isRootNode()) {
                    this.collection.add(breadcrumb);
                }
            },

            initialize: function (options) {
                var that = this;
                this.collection = new BreadcrumbCollection([{action: 'home', url: '#', title: 'Home'}]);
                var NavViewClass = makeLayout();
                var navView = new NavViewClass({collection: this.collection});

                // the initial url match needs fixing
                // won't match urls like #servers/filter/active
                var urlFragment = Backbone.history.fragment;
                var navItemModel = this.collection.where({ action: urlFragment })[0];

                navItemModel && navItemModel.setActive(true);

                options.Vent.on("route:changed", function(route, url) {
                    that.onRouteChange(route, url);
                });

                options.Vent.trigger("layout:show", "navigation", navView);
            }
        });
});