﻿/**
 * Module Event Aggregator
 * handles event passing within the module
 */
define(['marionette'], function (Marionette) {
    return new Backbone.Wreqr.EventAggregator();
});
