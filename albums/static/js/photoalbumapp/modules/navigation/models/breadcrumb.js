﻿define(["marionette"], function (Marionette) {
    return Backbone.Model.extend({
        defaults: {
            url: '',
            active: false,
            title: 'default title'
        },
        setActive: function (state) {
            this.set("active", !!state);
        },
        isRootNode: function() {
            var action = this.get('action');
            return (action == 'home' || action == '');
        }
    });
});