﻿define(["marionette", "text!../templates/item.html", "text!../templates/dropdown.html"], function (Marionette, itemTpl, dropdownTpl) {
    return Marionette.ItemView.extend({
        getTemplate: function () {
            if (this.model.isDropdown()) {
                this.$el.addClass('has-dropdown not-click');
                return _.template(dropdownTpl);
            }
            return _.template(itemTpl);
        },
        tagName: "li",
        triggers: {
            "click": "menu:item:clicked"
        },
        modelEvents: {
            "change:active": "activeChanged"
        },
        activeChanged: function (model, active) {
            this.markActive(active);
        },
        markActive: function (state) {
            if (state) {
                this.$el.addClass("active");
            }
            else {
                this.$el.removeClass("active");
            }
        },
        onBeforeRender: function () {
            var active = this.model.get('active');
            this.markActive(active);
        }
    });
});