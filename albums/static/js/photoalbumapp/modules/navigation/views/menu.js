﻿define(["marionette", "./item"], function (Marionette, MenuItem) {
    return Marionette.CollectionView.extend({
        lastActiveItem: null,
        childView: MenuItem,
        tagName: 'ul',
        childEvents: {
            "menu:item:clicked": function (eventName, item, e) {
                this.trigger("menu:item:clicked", item);
            }
        },
        setActiveItem: function (itemModel) {
            if (this.lastActiveItem !== null) {
                this.lastActiveItem.setActive(false);
            }

            itemModel.setActive(true);
            this.lastActiveItem = itemModel;
        }
    });
});