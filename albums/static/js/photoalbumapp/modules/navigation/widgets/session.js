﻿define(["marionette", "../views/menu", "../collections/session"], function (Marionette, Menu, SessionCollection) {
    /**
     * returns an instance of a Menu View coupled with a Collection
     * for now there's not much more here, 
     * just an easy way to have a reference so we can use the same instance in different files
     */
    return new Menu({
        collection: SessionCollection
    });
});