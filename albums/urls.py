from django.conf.urls import patterns, url, include
from rest_framework import routers
from api import AlbumViewSet, ImageViewSet

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'albums', AlbumViewSet)
router.register(r'images', ImageViewSet)


urlpatterns = patterns('albums.views',
	url(r'^api/', include(router.urls)),
	url(r'^$', 'home_view'),
)