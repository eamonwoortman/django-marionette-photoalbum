from rest_framework import serializers
from rest_framework import viewsets
from models import Album, Image

class AlbumSerializer(serializers.ModelSerializer):
    num_images = serializers.Field(source='image_count')

    class Meta:
        model = Album
        fields = ('id', 'title', 'created', 'num_images')

class ImageSerializer(serializers.ModelSerializer):
    #for our user, select the username field of the user object
    user = serializers.SlugRelatedField(many=False, read_only=True, slug_field='username')
    class Meta:
        model = Image


class AlbumViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Album.objects.all()
    serializer_class = AlbumSerializer


class ImageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Image.objects.all()
    def get_queryset(self):
        album_id = self.request.QUERY_PARAMS.get('album_id', None)
        if album_id != None:
            return Image.objects.filter(albums=album_id)

        return Image.objects.all()

    serializer_class = ImageSerializer

