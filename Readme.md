### How to install:

1. Install Python 2.7.
2. Install [PIP](http://www.pip-installer.org/en/latest/installing.html#install-or-upgrade-pip).
3. Optionally install VirtualEnv and create a virtual environment.
4. Cd to the photoalbum folder(where 'requirements.txt' is located)
5. Pip install the required packages using: `pip install -r requirements.txt`.


### Run the webserver:
1. Test the project by executing: `python manage.py test`.
2. Execute the following cmd: `python manage.py runserver`
