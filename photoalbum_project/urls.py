import settings
from django.conf.urls.static import static
from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    #our albums app
    url(r'^', include('albums.urls')),
    url(r'^admin/', include(admin.site.urls)),
	
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
